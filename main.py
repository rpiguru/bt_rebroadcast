"""
- Show status of whether or not it's paired with iOS device
- SHow a list of the BT headphones in range
- pair/unpair with BT headphones.
- Start/Stop streaming received audio to one or more of the paired BT headphones
"""
import os
import time
import pexpect
import subprocess
import sys
import re


class BluetoothctlError(Exception):
    """This exception is raised, when bluetoothctl fails to start."""
    pass


class Bluetoothctl:
    """A wrapper for bluetoothctl utility."""

    def __init__(self):
        out = subprocess.check_output("rfkill unblock bluetooth", shell=True)
        self.child = pexpect.spawn("bluetoothctl", echo=False)
        self.make_discoverable()
        self.start_agent()

    def get_output(self, command, pause=0):
        """Run a command in bluetoothctl prompt, return output as a list of lines."""
        self.child.send(command + "\n")
        time.sleep(pause)
        try:
            start_failed = self.child.expect(["bluetooth", pexpect.EOF])
        except pexpect.exceptions.TIMEOUT:
            return None

        if start_failed:
            raise BluetoothctlError("Bluetoothctl failed after running " + command)

        return self.child.before.split("\r\n")

    def start_scan(self):
        """Start bluetooth scanning process."""
        try:
            out = self.get_output("scan on")
        except BluetoothctlError, e:
            print(e)
            return None

    def make_discoverable(self):
        """Make device discoverable."""
        try:
            out = self.get_output("discoverable on")
        except BluetoothctlError, e:
            print(e)
            return None

    def parse_device_info(self, info_string):
        """Parse a string corresponding to a device."""
        device = {}
        block_list = ["[\x1b[0;", "removed"]
        string_valid = not any(keyword in info_string for keyword in block_list)

        if string_valid:
            try:
                device_position = info_string.index("Device")
            except ValueError:
                pass
            else:
                if device_position > -1:
                    attribute_list = info_string[device_position:].split(" ", 2)
                    device = {
                        "mac_address": attribute_list[1],
                        "name": attribute_list[2]
                    }

        return device

    def get_available_devices(self):
        """Return a list of tuples of paired and discoverable devices."""
        try:
            out = self.get_output("devices")
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            available_devices = []
            if out is not None:
                for line in out:
                    device = self.parse_device_info(line)
                    if device:
                        available_devices.append(device)

            return available_devices

    def get_paired_devices(self):
        """Return a list of tuples of paired devices."""
        try:
            out = self.get_output("paired-devices")
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            paired_devices = []
            if out is not None:
                for line in out:
                    device = self.parse_device_info(line)
                    if device:
                        paired_devices.append(device)

            return paired_devices

    def get_discoverable_devices(self):
        """Filter paired devices out of available."""
        available = self.get_available_devices()
        paired = self.get_paired_devices()
        return [d for d in available if d not in paired]

    def get_device_info(self, mac_address):
        """Get device info by mac address."""
        try:
            out = self.get_output("info " + mac_address)
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            return out

    def get_connectable_devices(self):
        """Get a  list of connectable devices.
        Must install 'sudo apt-get install bluez blueztools' to use this"""
        try:
            res = []
            out = subprocess.check_output(["hcitool", "scan"])  # Requires 'apt-get install bluez'
            out = out.split("\n")
            device_name_re = re.compile("^\t([0-9,:,A-F]{17})\t(.*)$")
            for line in out:
                device_name = device_name_re.match(line)
                if device_name is not None:
                    res.append({
                        "mac_address": device_name.group(1),
                        "name": device_name.group(2)
                    })
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            return res

    def is_connected(self):
        """Returns True if there is a current connection to any device, otherwise returns False"""
        try:
            res = False
            out = subprocess.check_output(["hcitool", "con"])  # Requires 'apt-get install bluez'
            out = out.split("\n")
            mac_addr_re = re.compile("^.*([0-9,:,A-F]{17}).*$")
            for line in out:
                mac_addr = mac_addr_re.match(line)
                if mac_addr is not None:
                    res = True
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            return res

    def pair(self, mac_address, pin_code=None):
        """Try to pair with a device by mac address."""
        print "Pairing to ", mac_address
        try:
            if pin_code is None:
                out = self.get_output("pair " + mac_address, 4)
                res = self.child.expect(["Failed to pair", "Pairing successful", pexpect.EOF])
                success = True if res == 1 else False
                return success
            else:
                print("pair " + mac_address)
                self.child.send("pair " + mac_address + "\n")
                print "Please input pin code on your iPhone"
                time.sleep(5)

                start_failed = self.child.expect(["PIN", pexpect.EOF])
                if start_failed:
                    raise BluetoothctlError("Bluetoothctl failed after running " + "pair " + mac_address)
                self.child.sendline(pin_code)
                res = self.child.expect(["Failed to pair", "Pairing successful", pexpect.EOF])
                print self.child.after
                success = True if res == 1 else False
                return success
        except BluetoothctlError, e:
            print(e)
            return None
        except pexpect.exceptions.TIMEOUT:
            return None

    def remove(self, mac_address):
        """Remove paired device by mac address, return success of the operation."""
        try:
            out = self.get_output("remove " + mac_address, 3)
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            res = self.child.expect(["not available", "Device has been removed", pexpect.EOF])
            success = True if res == 1 else False
            return success

    def connect(self, mac_address):
        """Try to connect to a device by mac address."""
        print "Connecting to ", mac_address
        try:
            out = self.get_output("connect " + mac_address, 2)
            res = self.child.expect(["Failed to connect", "Connection successful", pexpect.EOF])
            success = True if res == 1 else False
            return success
        except BluetoothctlError, e:
            print(e)
            return None
        except pexpect.exceptions.TIMEOUT:
            return None

    def disconnect(self, mac_address):
        """Try to disconnect to a device by mac address."""
        try:
            out = self.get_output("disconnect " + mac_address, 2)
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            res = self.child.expect(["Failed to disconnect", "Successful disconnected", pexpect.EOF])
            success = True if res == 1 else False
            return success

    def trust(self, mac_address):
        """Trust the device with the given MAC address"""
        try:
            out = self.get_output("trust " + mac_address, 4)
        except BluetoothctlError, e:
            print(e)
            return None
        else:
            res = self.child.expect(["not available", "trust succeeded", pexpect.EOF])
            success = True if res == 1 else False
            return success

    def start_agent(self):
        """Start agent"""
        try:
            out = self.get_output("agent on")
        except BluetoothctlError, e:
            print(e)
            return None

    def stop_agent(self):
        """Start agent"""
        try:
            out = self.get_output("agent off")
        except BluetoothctlError, e:
            print(e)
            return None

    def default_agent(self):
        """Start default agent"""
        try:
            out = self.get_output("default-agent")
        except BluetoothctlError, e:
            print(e)
            return None


def count_down(s):
    for i in range(s, 0, -1):
        print i,
        sys.stdout.flush()
        time.sleep(1)
    print


def display_list(l_list):
    """
    Display a list on screen.
    """
    for i in range(len(l_list)):
        dev = l_list[i]
        print "  ", str(i + 1), "    Name: ", dev.get('name').ljust(25), "MAC: ", dev.get('mac_address')
    print ""


if __name__ == "__main__":

    print("Initializing bluetooth...")
    bl = Bluetoothctl()
    bl.make_discoverable()

    print "Command list:"
    print "scan,x: scan nearby devices for x seconds and display them."
    print "pair: pair with the device."
    print "connect: connect with the device."
    print "status: display paired devices."
    print "start: start broadcasting."
    print "CTRL-C: exit."

    while True:
        try:
            cmd = raw_input("Enter command: ")
        except KeyboardInterrupt:
            print "\nExiting..."
            exit(0)

        if 'scan' in cmd:
            interval = 0
            try:
                interval = int(cmd.split(',')[1])
            except IndexError:
                print "Please input time."
                continue
            except ValueError:
                print "Invalid command type."
                continue
            bl.start_scan()

            print "Scanning for " + str(interval) + " seconds..."
            count_down(interval)
            print "Discoverable devices:"
            display_list(bl.get_discoverable_devices())

        elif cmd == 'pair':
            l_list = bl.get_discoverable_devices()
            print "Discovered device list:"
            display_list(l_list)
            try:
                val = raw_input("Enter number: ")
            except KeyboardInterrupt:
                print "\nExiting..."
                continue

            dev = None
            try:
                num = int(val)
                dev = l_list[num - 1]
            except IndexError:
                print "Index out of scope. Please input correct number"
                continue
            except ValueError:
                print "Invalid type."
                continue

            pair_type = raw_input("Is this iPhone? Yes(y), No(n)   :")

            if pair_type in ['Y', 'y']:
                pin_code = raw_input("Enter PIN code(4 digits only):")
            else:
                pin_code = None

            bl.trust(dev.get('mac_address'))
            result = bl.pair(dev.get('mac_address'), pin_code)
            if result:
                print "Pairing successful. Please connect then."
            else:
                print "Failed to pair. Please try again later."

        elif cmd == 'remove':
            l_list = bl.get_paired_devices()
            print "Paired device list:"
            display_list(l_list)
            try:
                val = raw_input("Enter number: ")
            except KeyboardInterrupt:
                print "\nExiting..."
                continue

            dev = None
            try:
                num = int(val)
                dev = l_list[num - 1]
            except IndexError:
                print "Index out of scope. Please input correct number"
                continue
            except ValueError:
                print "Invalid type."
                continue

            result = bl.remove(dev.get('mac_address'))
            if result:
                print "Removed successfully."
            else:
                print "Failed to remove. Please try again later."

        elif 'connect' in cmd:
            l_list = bl.get_paired_devices()
            if len(l_list) == 0:
                print "No paired device"
                continue
            print "Paired device list:"

            display_list(l_list)

            try:
                val = raw_input("Enter number: ")
            except KeyboardInterrupt:
                print "\nExiting..."
                continue

            dev = None
            try:
                num = int(val)
                dev = l_list[num - 1]
            except IndexError:
                print "Index out of scope. Please input correct number"
                continue
            except ValueError:
                print "Invalid type."
                continue

            print "Connecting to ", dev.get('name'), "...  Please allow connection on it."

            result = bl.connect(dev.get('mac_address'))
            if result:
                print "Connected successfully."
            else:
                print "Failed to connect. Please try again later."

        elif cmd == 'status':
            print "Getting paired devices..."
            display_list(bl.get_paired_devices())

        elif cmd == 'start':
            print "Start broadcasting..."
            cmd_result = os.popen("sudo pactl list sources short")
            source_list = []
            for line in cmd_result:
                fields = line.split()
                if 'alsa' not in fields[1] and 'bluez_source' in fields[1]:
                    source_list.append(fields[1])

            if len(source_list) == 0:
                print "Error, Cannot find iPhone. Please connect now..."
            else:
                print "Found BT source: ", source_list[0]
                cmd_result = os.popen("sudo pactl list sinks short")
                sink_list = []
                for line in cmd_result:
                    fields = line.split()
                    if 'alsa' not in fields[1] and 'bluez_sink' in fields[1]:
                        sink_list.append(fields[1])

                if len(sink_list) == 0:
                    print "Error, Cannot find sink. Please connect BT speakers now."
                else:
                    for sink in sink_list:
                        cmd_start = "sudo pactl load-module module-loopback source=" + source_list[0] + " sink=" + sink
                        cmd_result = os.popen(cmd_start)
                        for line in cmd_result:
                            fields = line.split()
                            try:
                                re = int(fields[0])
                                if re > 0:
                                    print "Success to connect with ", sink
                                else:
                                    print "Failed to connect, please try again later."
                            except ValueError:
                                print "Failed to connect, please try again later."

        else:
            print "Invalid command.\n"

        # Initialize instance.  - Important.
        bl.__init__()


